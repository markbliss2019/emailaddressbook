﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace EmailAddressBook
{
    class PersonEntry : INotifyPropertyChanged
    {
        private const string filename = "Phonebook.txt";
        private string personName, personEmail, personPhoneNumber, filestring;
        public static string[] filedata;

        private ObservableCollection<DataField> phoneBookName = new ObservableCollection<DataField>();
        public ObservableCollection<DataField> PhoneBook
        {
            get { return phoneBookName; }
            set
            {
                phoneBookName = value;
                OnPropertyChanged();
                //    OnPropertyChanged();// duplicate
            }
        }

        public PersonEntry()
        {
            LoadDataFromFile();
        }

        #region properties
        public string PersonName
        {
            get { return personName; }
            set
            {
                if (personName != value)
                {
                    personName = value;
                    OnPropertyChanged();
                }
            }
        }

         public string Personemail
        {
            get { return personEmail; }
            set
            {
                if (personEmail != value)
                {
                    personEmail = value;
                    OnPropertyChanged();
                }
            }
        }
        public string PersonPhoneNumber
        {
            get { return personPhoneNumber; }
            set
            {
                if (personPhoneNumber != value)
                {
                    personPhoneNumber = value;
                    OnPropertyChanged();
                }
            }
        }
        #endregion

        public void LoadDataFromFile()
        {
            string workingDirectory = Environment.CurrentDirectory;     // This will get the current WORKING directory (i.e. \bin\Debug)             
            string projectDirectory = Directory.GetParent(workingDirectory).Parent.FullName;     // or: Directory.GetCurrentDirectory() gives the same result // This will get the current PROJECT directory 
            string filepath = Path.Combine(projectDirectory, filename);

            //     try
            //    {
            using (StreamReader sr = new StreamReader(filepath))
            {
                PhoneBook.Clear();
                var x = sr.ReadToEnd();
                filedata = x.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

                for (int i = 0; i < filedata.Length; i++)
                {
                    //string name = filedata[i].Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries).ToString();
                    string[] name = filedata[i].Split('\n');
                    PhoneBook.Add(new DataField() { NameList = name[0] });
                }
            }
   //         }
   /*         catch (FileNotFoundException e)
            {
                if (e.Source != null)
                {
                    MessageBox.Show(e.Message);
                }
            }*/





        }
        public void getSaveData()
        {
            filestring = personName + "\n" + personEmail + "\n" + personPhoneNumber;
            WriteResToFile(filestring);
            LoadDataFromFile();
        }

        private string project_Path
        {
            get
            {
                return System.IO.Path.GetDirectoryName(System.IO.Path.GetDirectoryName(Directory.GetCurrentDirectory()));
            }
        }

        private string fullResultFilePath
        {
            get
            {
                return project_Path + "\\" + filename;
            }
        }
        private void WriteResToFile(string stringToFile)
        {
            StreamWriter sw = new StreamWriter(fullResultFilePath, true);
            sw.WriteLine(stringToFile);
            sw.Close();
        }

        #region onpropertychange
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([System.Runtime.CompilerServices.CallerMemberName]string prop = "")
        {
            PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
        #endregion

    }
}
