﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EmailAddressBook
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    
    public partial class MainWindow : Window
    {

        PersonEntry obj_phoneBookVmClass = new PersonEntry();
        SecondWindow obj_SecondForm;

        public static MainWindow AppWindow;
        public static int selectedItemIndex;


        public MainWindow()
        {
            InitializeComponent();
            DataContext = obj_phoneBookVmClass;
            AppWindow = this;

        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
           
        }

        private void Btndelete_Click(object sender, RoutedEventArgs e)
        {

            
        }

        private void Lbnamelist_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            txtselectedName.Text = lbnamelist.Items[lbnamelist.SelectedIndex].ToString();
            selectedItemIndex = lbnamelist.SelectedIndex;
            obj_SecondForm = new SecondWindow();
            obj_SecondForm.Show();
        }
    }
}
