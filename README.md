How To Build the Install and use Project

1. Open Visual Studio Community 2017

2. OPEN Project "EmailAddressBook" under directory C:\Users\tazzy\source\repos\EmailAddressBook and select file EmailAddressBook.sln

3. Once project opened Set the solution configuration to release mode and then build it. 

4. Test the released build by going to the C:\Users\tazzy\source\repos\EmailAddressBook\EmailAddressBook\bin\Release and running EmailAddressBook.exe with phonebook.txt to make sure the program runs as per requirements

**** License *****

License.md is the license file needed for this project. It was my choice to require license to use this software

as to be able to continue to do updates and to control distribution of this software in the future.